define({
  "name": "Sharee",
  "version": "0.1.0",
  "description": "The API documentation about Sharee",
  "title": "Sharee - API Documentation",
  "url": "https://sharee-api.herokuapp.com",
  "header": {
    "title": "Quick Links",
    "content": "<h2>Quick Links</h2>\n<ul>\n<li><a href=\"https://sharee-api.herokuapp.com\">Sharee API</a></li>\n</ul>\n"
  },
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2017-03-26T00:11:21.842Z",
    "url": "http://apidocjs.com",
    "version": "0.16.1"
  }
});
