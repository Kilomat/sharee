/**
 * Created by BAHA on 29/03/2017.
 */

module.exports = function (app) {
    /* API ROUTING */

    app.use('/users', require('./routes/api/user'));
    app.use('/token', require('./routes/api/token'));
};
